#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Dev: wh0ami
Licence: Public Domain <https://unlicense.org>
Project: https://codeberg.org/wh0ami/hc4-oled/
"""

# importing dependencies
import subprocess
from multiprocessing import cpu_count
from os import path, getloadavg
from time import sleep, time

import mdstat
from PIL import ImageFont
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306
from psutil import boot_time


def main():
    # initialize oled display
    device = ssd1306(i2c(port=0, address=0x3C), rotate=2)

    # initialize font
    font = ImageFont.truetype(
        path.abspath(path.join(path.dirname(__file__), 'fonts', 'DejaVuSansMono.ttf')),
        size=17
    )

    # read serial numbers of the hard drives
    raid = mdstat.parse()
    disk_serial = {}
    for disk in raid['devices']['md0']['disks']:
        cmd = f"/lib/udev/scsi_id --page=0x80 --whitelisted --device=/dev/{disk} -x | " \
              f"awk -F'=' '/ID_SERIAL_SHORT/ {{print $2;}}'"
        with subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE) as process:
            output, error = process.communicate()
            disk_serial[disk] = str(output.decode('UTF-8')).strip()

    # method for displaying a string on the oled display
    def print_string_on_display(string):
        with canvas(device) as draw:
            try:
                draw.text((0, 0), string, fill="white", font=font)
            except IOError:
                pass

    # infinite loop until the process is getting stopped
    while True:
        # sleep value between different content in seconds
        pause = 5

        # read current raid status from /proc/mdstat via mdstat library
        raid = mdstat.parse()
        # iterate over all disks of md0 and request the disks serial number from the kernel
        # each disk has a own view with its raid status (faulty or not)
        for disk in raid['devices']['md0']['disks']:
            result = f'+++ Disk +++\n• {disk_serial[disk]}\n• '
            if raid['devices']['md0']['disks'][disk]['faulty']:
                print_string_on_display(f'{result}! faulty !')
            else:
                print_string_on_display(f'{result}not faulty')
            sleep(pause)

        # get the current raid status and display it
        if raid['devices']['md0']['resync'] is not None:
            print_string_on_display(
                f"+++ RAID +++\n• {raid['devices']['md0']['resync']['operation']}\n"
                f"• {raid['devices']['md0']['resync']['progress']}"
            )
        else:
            print_string_on_display('+++ RAID +++\n• synced')
        sleep(pause)

        # print system load
        load = getloadavg()
        print_string_on_display(
            f'Load1:  {round(load[0], 2)}\nLoad5:  {round(load[1], 2)}\n'
            f'Load15: {round(load[2], 2)}')
        sleep(pause)

        # print uptime
        up_seconds = int(time() - boot_time())
        up_days = up_seconds // 86400
        up_hours = (up_seconds % 86400) // 3600
        if up_hours < 10:
            up_hours = f'0{up_hours}'
        up_minutes = ((up_seconds % 86400) % 3600) // 60
        if up_minutes < 10:
            up_minutes = f'0{up_minutes}'
        print_string_on_display(f'+ Uptime +\n• {up_days} days\n  and {up_hours}:{up_minutes}h')
        sleep(pause)

        # print cpu temperature and freq
        with open('/sys/class/thermal/thermal_zone0/'
                  'hwmon0/temp1_input', encoding='UTF-8') as thermal_file:
            temp = str(int(round(int(thermal_file.read()) / 1000, 0)))
            thermal_file.close()

        # the clock rate is calculated as a average over all cores
        corenum = cpu_count()
        freqsum = 0
        for core_index in range(corenum):
            with open(f'/sys/devices/system/cpu/'
                      f'cpu{core_index}/cpufreq/cpuinfo_cur_freq', encoding='UTF-8') as core_file:
                freq = int(core_file.read())
                core_file.close()
            freqsum += freq
        freq = str(int(round(freqsum / 1000 / corenum, 0)))

        print_string_on_display(f'+++ CPU +++\n• {temp}°C\n• {freq} MHz')
        sleep(pause)


# main method
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
